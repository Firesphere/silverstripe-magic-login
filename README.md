# Magic link login for Silverstripe 4/5
![Tests](https://ci.codeberg.org/api/badges/13104/status.svg)

This module aims to provide a "log in with a magic link" via email,
same as e.g. Slack does.

It adds an authenticator and login form, you might need to update some log-in
form styling so the tabbed authenticator works and shows properly.

## Disclaimer

If this module breaks your website, you get to keep all the pieces.

## Installation

`composer require firesphere/magiclogin`

## Configuration

```yaml
---
name: mytokenlogin
after: 
    - TokenLogin
---
SilverStripe\Core\Injector\Injector:
  Firesphere\MagicLogin\Config\TokenConfig:
    properties:
      TokenLifetime: {time in minutes}
      PersistentLogin: { false|true }
      SameBrowser: { false|true }
      AdminAccess: { false| true }
```

- TokenLifetime: Defines how long the token should be valid for. Default is 30 minutes
- PersistentLogin: Should the user be logged in as if the checkbox "Remember me" is set
- SameBrowser: Is the token only valid for the same browser. This will set a cookie and a session token, which will be validated, on top of the one-time url token
- AdminAccess: Allow or deny magic link logins to the CMS (default false)

## Usage

After styling the tabbed log in forms properly, the user
can log in by entering their email, and requesting a magic
link.

The user will receive a magic link in their email, assuming the
email address is found.

When the user follows this link, the user is logged in

## WARNING

If the user uses an email provider that follows links, the log-in token
might be expired due to the provider pre-fetching the link.

I've added a forbidden to some known bots (Bing/Outlook/Google/Slack), but
I can't promise it's completely works.

# License:

LGPL 3.0 or later

# Cow?

Pictured below is a cow, just for you.
```

               /( ,,,,, )\
              _\,;;;;;;;,/_
           .-"; ;;;;;;;;; ;"-.
           '.__/`_ / \ _`\__.'
              | (')| |(') |
              | .--' '--. |
              |/ o     o \|
              |           |
             / \ _..=.._ / \
            /:. '._____.'   \
           ;::'    / \      .;
           |     _|_ _|_   ::|
         .-|     '==o=='    '|-.
        /  |  . /       \    |  \
        |  | ::|         |   | .|
        |  (  ')         (.  )::|
        |: |   |;  U U  ;|:: | `|
        |' |   | \ U U / |'  |  |
        ##V|   |_/`"""`\_|   |V##
           ##V##         ##V##
```
