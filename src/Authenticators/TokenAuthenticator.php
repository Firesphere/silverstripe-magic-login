<?php

namespace Firesphere\MagicLogin\Authenticators;

use Firesphere\MagicLogin\Config\TokenConfig;
use Firesphere\MagicLogin\Controllers\TokenLoginHandler;
use Firesphere\MagicLogin\Extensions\MemberExtension;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use SilverStripe\Control\Cookie;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\FieldType\DBDatetime;
use SilverStripe\ORM\ValidationException;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Authenticator;
use SilverStripe\Security\IdentityStore;
use SilverStripe\Security\Member;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\Permission;

/**
 * @class Firesphere\MagicLogin\Authenticators\TokenAuthenticator
 *
 *
 */
class TokenAuthenticator extends MemberAuthenticator
{
    /**
     * @return int
     */
    public function supportedServices()
    {
        // Bitwise-OR of all the supported services in this Authenticator, to make a bitmask
        return Authenticator::LOGIN;
    }

    /**
     * @inheritDoc
     * @param array $data
     * @param HTTPRequest $request
     * @param mixed $result
     * @return MemberExtension|Member|null
     * @throws NotFoundExceptionInterface
     * @throws ValidationException
     */
    public function authenticate(array $data, HTTPRequest $request, &$result = null)
    {
        /** @var TokenConfig $config */
        $config = Injector::inst()->get(TokenConfig::class);
        /** @var LoggerInterface $logger */
        $logger = Injector::inst()->get(LoggerInterface::class);
        /** @var ValidationResult $result */
        $result = $result ?: Injector::inst()->get(ValidationResult::class);
        if ($config->isSameBrowser()) {
            // Get the cookie and session tokens, and destroy them immediately
            $cookieToken = Cookie::get(TokenLoginHandler::SECURITY_TOKEN) ?? 'nocookie' . uniqid('', true);
            $sessionToken = $request->getSession()->get(TokenLoginHandler::SECURITY_TOKEN) ?? 'nosession' . uniqid('', false);
            // Fallback strings above can not ever be the same, that's why they are hardcoded. Uniqid isn't cryptographically safe, but the length won't ever match
            if (!hash_equals($cookieToken, $sessionToken)) {
                // Setting the error will prevent any success downstream without incurring noticable timing issues.
                // This error message is vague on purpose!
                $result->addError(_t(
                    'Firesphere\\MagicLogin\\Authenticators\\TokenAuthenticator.TOKENMISMATCH',
                    'An unknown error occurred'
                ));
                $logger->error('User cookie and session tokens do not match. Continuing process to avoid timing attacks');
            }


            Cookie::force_expiry(TokenLoginHandler::SECURITY_TOKEN);
            $request->getSession()->clear(TokenLoginHandler::SECURITY_TOKEN);
        }
        /** @var IdentityStore $identityStore */
        $identityStore = Injector::inst()->get(IdentityStore::class);


        /** @var Member|MemberExtension|null $member */
        $member = Member::get()
            ->filter([
                'LoginToken'              => $data['token'] ?? null,
                'TokenExpiry:GreaterThan' => DBDatetime::now()
            ])
            ->first();

        if ($member) {
            // If we have a member, immediately invalidate this token
            $member->LoginToken = '';
            $member->TokenExpiry = '1970-01-01 00:00:00';
            $member->write();

            $member->validateCanLogin($result);
            $data['Email'] = $member->Email; // Required for login recording
            // Optionally record every login attempt as a {@link LoginAttempt} object
            $this->recordLoginAttempt($data, $request, $member, $result->isValid());

            if ($result->isValid()) { // It can not be valid if the cookie and token don't match above
                $identityStore->logIn($member, $config->isPersistentLogin(), $request);
                $member->registerSuccessfulLogin();
                if (!$config->isAdminAccess() && Permission::checkMember($member, 'CMS_ACCESS')) {
                    // Block CMS access via magic token if required so
                    $result->addError(_t(
                        'Firesphere\\MagicLogin\\Authenticators\\TokenAuthenticator.NOADMIN',
                        'CMS Users can not log in with a magic token'
                    ));
                }

                return $member;
            }
            // Force-log-out the user, cookie and token may have been spoofed.
            $identityStore->logOut();
            $logger->error(sprintf('User could not log-in. User ID: %d', $member->ID));

            $member->registerFailedLogin();
        }
        // A non-existing member occurred. This will make the result "valid" so let's invalidate
        $result->addError(_t(
            'Firesphere\\MagicLogin\\Authenticators\\TokenAuthenticator.ERROR',
            "There seems to be something wrong with the token, you could not be logged in."
        ));

        return null;
    }

    /**
     * @param $link
     * @return TokenLoginHandler
     */
    public function getLoginHandler($link): TokenLoginHandler
    {
        return TokenLoginHandler::create($link, $this);
    }

    /**
     * @param $handler
     * @param $member
     * @return string
     */
    public static function getTokenLink($handler, $member)
    {
        $link = sprintf('%s?token=%s', $handler->Link('token'), $member->LoginToken);

        return sprintf('%s%s', Director::absoluteBaseURL(), $link);
    }
}
