<?php

namespace Firesphere\MagicLogin\Config;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Injector\Injectable;

/**
 * class Firesphere\MagicLogin\Config\TokenConfig
 *
 * A config class for Tokenized login
 */
class TokenConfig
{
    use Configurable;
    use Injectable;

    /**
     * @var int
     */
    private $tokenLifetime = 30;

    /**
     * @var bool
     */
    private $persistentLogin = false;

    /**
     * @var bool
     */
    private $sameBrowser = true;

    /**
     * @var bool
     */
    private $adminAccess = false;

    /**
     * @return bool
     */
    public function isAdminAccess(): bool
    {
        return $this->adminAccess;
    }

    /**
     * @param bool $adminAccess
     * @return void
     */
    public function setAdminAccess(bool $adminAccess): void
    {
        $this->adminAccess = $adminAccess;
    }

    /**
     * @return int
     */
    public function getTokenLifetime(): int
    {
        return $this->tokenLifetime;
    }

    /**
     * @param int $tokenLifetime
     * @return void
     */
    public function setTokenLifetime(int $tokenLifetime): void
    {
        $this->tokenLifetime = $tokenLifetime;
    }

    /**
     * @return bool
     */
    public function isPersistentLogin(): bool
    {
        return $this->persistentLogin;
    }

    /**
     * @param bool $persistentLogin
     * @return void
     */
    public function setPersistentLogin(bool $persistentLogin): void
    {
        $this->persistentLogin = $persistentLogin;
    }

    /**
     * @return bool
     */
    public function isSameBrowser(): bool
    {
        return $this->sameBrowser;
    }

    /**
     * @param bool $sameBrowser
     * @return void
     */
    public function setSameBrowser(bool $sameBrowser): void
    {
        $this->sameBrowser = $sameBrowser;
    }
}
