<?php

namespace Firesphere\MagicLogin\Controllers;

use Firesphere\MagicLogin\Authenticators\TokenAuthenticator;
use Firesphere\MagicLogin\Config\TokenConfig;
use Firesphere\MagicLogin\Extensions\MemberExtension;
use Firesphere\MagicLogin\Forms\TokenLoginForm;
use Psr\Container\NotFoundExceptionInterface;
use Random\RandomException;
use SilverStripe\Control\Cookie;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\ValidationException;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\LoginForm;
use SilverStripe\Security\Member;
use SilverStripe\Security\MemberAuthenticator\LoginHandler;
use SilverStripe\Security\MemberAuthenticator\LogoutHandler;
use SilverStripe\Security\MemberAuthenticator\MemberAuthenticator;
use SilverStripe\Security\Security;

/**
 * Class \Firesphere\MagicLogin\Controllers\TokenLoginHandler
 *
 */
class TokenLoginHandler extends LoginHandler
{
    /**
     * Name of cookie/session variable for hardening
     */
    public const SECURITY_TOKEN = 'security_token';
    /**
     * @var string[]
     */
    private static $allowed_actions = [
        'LoginForm',
        'token'
    ];

    /**
     * @param $link
     * @param TokenAuthenticator $authenticator
     * @throws HTTPResponse_Exception
     */
    public function __construct($link, TokenAuthenticator $authenticator)
    {
        parent::__construct($link, $authenticator);
        $agent = $this->getRequest()->getHeader('user-agent') ?? '';
        if (stripos($agent, 'BingPreview') !== false ||
            stripos($agent, 'Slackbot') !== false ||
            stripos($agent, 'googlebot') !== false ||
            stripos($agent, 'bot') !== false
        ) {
            $this->httpError(403, 'NO BOTS ALLOWED');
            exit(1);
        }

    }

    /**
     * @param HTTPRequest $request
     * @return HTTPResponse
     * @throws NotFoundExceptionInterface
     * @throws HTTPResponse_Exception
     * @throws ValidationException
     */
    public function token(HTTPRequest $request)
    {
        $vars = $request->getVars();
        $vars['token'] = $vars['token'] ?? uniqid('incorrect_token', false);

        /** @var TokenAuthenticator $authenticator */
        $authenticator = Injector::inst()->get(TokenAuthenticator::class);

        $member = $authenticator->authenticate($vars, $request, $result);

        if ($member) {
            if ($result->isValid()) {
                // Absolute redirection URLs may cause spoofing
                $backURL = $this->getBackURL() ?: '/';

                // If a default login dest has been set, redirect to that.
                $backURL = Security::config()->get('default_login_dest') ?: $backURL;

                return $this->redirect($backURL);
            }
            Injector::inst()->get(LogoutHandler::class)->doLogOut($member);

            return $this->httpError(403);
        }

        return $this->httpError(403);
    }

    /**
     * @param $action
     * @return string
     */
    public function Link($action = null)
    {
        return parent::Link($action);
    }

    /**
     * @param $data
     * @param TokenLoginForm $form
     * @param HTTPRequest $request
     * @return HTTPResponse
     * @throws NotFoundExceptionInterface
     * @throws RandomException
     * @throws ValidationException
     */
    public function doSendToken($data, TokenLoginForm $form, HTTPRequest $request)
    {
        /** @var TokenConfig $conf */
        $conf = Injector::inst()->get(TokenConfig::class);

        // Attempt to identify user by email
        // Find user by email
        $identifierField = Member::config()->get('unique_identifier_field') ?? 'Email';
        /** @var Member|MemberExtension $member */
        $member = Member::get()
            ->filter([$identifierField => $data['Email'] ?? false])
            ->first();

        if (!$member) {
            // use a dummy member
            $member = Member::create(['Email' => 'dummy@example.com']);
        }
        if ($conf->isSameBrowser()) {
            // Set a token with the expected expiry
            // Cookie expects a time-out in days, so lets give it a part of a day
            $randomToken = uniqid('logintoken', true);
            Cookie::set(self::SECURITY_TOKEN, $randomToken, ((1 / 24) / 60) * $conf->getTokenLifetime());
            $request->getSession()->set(self::SECURITY_TOKEN, $randomToken);
        }

        $member->generateToken();

        if ($member->isInDB()) {
            $member->write();
        }

        $form->sessionMessage(_t(
            'Firesphere\\MagicLogin\\Controllers\\TokenLoginHandler.SUCCESS',
            "An email with a magic token has been sent, assuming an account with your username/email address exists."
        ), ValidationResult::TYPE_GOOD);

        $copy = Injector::inst()->create(MemberAuthenticator::class)->getLoginHandler('')->loginForm();
        $copy->sessionMessage(_t(
            'Firesphere\\MagicLogin\\Controllers\\TokenLoginHandler.SUCCESS',
            "An email with a magic token has been sent, assuming an account with your username/email address exists."
        ), ValidationResult::TYPE_GOOD);

        // Note, we can happily send to dummy@example.com, as it's null-routed

        $email = Email::create()
            ->setHTMLTemplate('Firesphere\\MagicLogin\\Email\\TokenLoginEmail')
            ->setData($member)
            ->setSubject(_t(
                'Firesphere\\MagicLogin\\Controllers\\TokenLoginHandler.MAILSUBJECT',
                'Your magic log-in link',
                'Email subject'
            ))
            ->addData('SigninLink', TokenAuthenticator::getTokenLink($this, $member))
            ->setTo($member->Email);
        if ($member->isInDB()) {
            $email->send();
        }

        return $form->getRequestHandler()->redirectBackToForm();
    }

    /**
     * Return the MemberLoginForm form
     *
     * @return TokenLoginForm
     */
    public function loginForm()
    {
        return TokenLoginForm::create(
            $this,
            get_class($this->authenticator),
            'LoginForm'
        );
    }

}
