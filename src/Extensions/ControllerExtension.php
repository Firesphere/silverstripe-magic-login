<?php

namespace Firesphere\MagicLogin\Extensions;

use Firesphere\MagicLogin\Authenticators\TokenAuthenticator;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Core\Extension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Security\Security;

/**
 * Class \Firesphere\MagicLogin\Extensions\ControllerExtension
 *
 * @property ContentController|ControllerExtension $owner
 */
class ControllerExtension extends Extension
{
    private static $allowed_actions = [
        'TokenLoginForm'
    ];
    public function TokenLoginForm()
    {
        $controller = Injector::inst()->get(Security::class);
        return Injector::inst()->get(TokenAuthenticator::class)
            ->getLoginHandler($controller->Link('login/token'))
            ->loginForm();
    }
}
