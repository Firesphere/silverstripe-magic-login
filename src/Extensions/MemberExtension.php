<?php

namespace Firesphere\MagicLogin\Extensions;

use Firesphere\MagicLogin\Config\TokenConfig;
use Psr\Container\NotFoundExceptionInterface;
use Random\RandomException;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\FieldType\DBDatetime;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\Security\Member;

/**
 * Class \Firesphere\MagicLogin\Extensions\MemberExtension
 *
 * @property Member|MemberExtension $owner
 * @property string $LoginToken
 * @property string $TokenExpiry
 */
class MemberExtension extends DataExtension
{
    private static $db = [
        'LoginToken'  => DBVarchar::class,
        'TokenExpiry' => DBDatetime::class
    ];

    /**
     * @return MemberExtension
     * @throws RandomException
     * @throws NotFoundExceptionInterface
     */
    public function generateToken()
    {
        $this->owner->LoginToken = bin2hex(string: random_bytes(45));
        /** @var TokenConfig $conf */
        $conf = Injector::inst()->get(TokenConfig::class);

        $this->owner->TokenExpiry = date('Y-m-d H:i:s', strtotime('+' . $conf->getTokenLifetime() . ' minutes'));

        return $this->owner;
    }
}
