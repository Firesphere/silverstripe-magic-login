<?php

namespace Firesphere\MagicLogin\Forms;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\Security\LoginForm;

class TokenLoginForm extends LoginForm
{
    public function __construct(
        $controller,
        $authenticatorClass,
        $name,
        $fields = null,
        $actions = null,
        $checkCurrentUser = true
    ) {
        $this->setController($controller);
        $this->setAuthenticatorClass($authenticatorClass);
        if (!$fields) {
            $fields = $this->getFormFields();
        }
        if (!$actions) {
            $actions = $this->getFormActions();
        }

        // Reduce attack surface by enforcing POST requests
        $this->setFormMethod('POST', true);

        parent::__construct($controller, $name, $fields, $actions);

        $this->setValidator(RequiredFields::create(['Email']));
    }

    /**
     * The name of this login form, to display in the frontend
     * Replaces Authenticator::get_name()
     *
     * @return string
     */
    public function getAuthenticatorName()
    {
        return _t('Firesphere\\MagicLogin\\Forms\\TokenLoginForm.TOKEN', "Magic Email Token");
    }

    protected function getFormFields()
    {
        return FieldList::create([
            HiddenField::create("AuthenticationMethod", null, $this->getAuthenticatorClass(), $this),

            TextField::create('Email', _t('Firesphere\\MagicLogin\\Forms\\TokenLoginForm.EMAIL', 'Email/Username'))
        ]);
    }

    protected function getFormActions()
    {
        return FieldList::create(
            FormAction::create('doSendToken', _t('Firesphere\\MagicLogin\\Forms\\TokenLoginForm.BUTTONTOKEN', "Request token"))
        );
    }
}
