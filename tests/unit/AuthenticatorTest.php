<?php

namespace Firesphere\MagicLogin\Tests;

use Firesphere\MagicLogin\Authenticators\TokenAuthenticator;
use Firesphere\MagicLogin\Controllers\TokenLoginHandler;
use SilverStripe\Control\NullHTTPRequest;
use SilverStripe\Control\Session;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\SapphireTest;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Authenticator;

class AuthenticatorTest extends SapphireTest
{
    /**
     * @var TokenAuthenticator
     */
    protected $service;

    protected function setUp(): void
    {
        $this->service = Injector::inst()->get(TokenAuthenticator::class);

        parent::setUp();
    }

    /**
     * Ensure that the supported feature is nothing but login
     * @return void
     */
    public function testGetSupportedServices()
    {
        $options = [
            Authenticator::LOGIN,
            Authenticator::LOGOUT,
            Authenticator::CHANGE_PASSWORD,
            Authenticator::RESET_PASSWORD,
            Authenticator::CMS_LOGIN,
            Authenticator::CHECK_PASSWORD
        ];
        foreach ($options as $possibility) {
            // Unless it's a LOGIN check, it should be false.
            if ($possibility !== Authenticator::LOGIN) {
                $this->assertNotEquals($possibility, $this->service->supportedServices());
            } else {
                $this->assertEquals($possibility, $this->service->supportedServices());
            }
        }
    }

    public function testAuthenticate()
    {
        $result = null;
        $nullRequest = new NullHTTPRequest();
        $nullRequest->setSession(new Session([]));
        $this->assertNull($this->service->authenticate([], $nullRequest, $result));
        $this->assertInstanceOf(ValidationResult::class, $result);
    }

    public function testGetLoginHandler()
    {
        $this->assertInstanceOf(TokenLoginHandler::class, $this->service->getLoginHandler('login'));
    }

    public function testGetTokenLink()
    {
        $handler = $this->service->getLoginHandler('login');
        $expected = 'login/Token';

        $this->assertEquals($expected, $handler->Link('Token'));
        $tmpMember = \SilverStripe\Security\Member::create(['Email' => 'woodpecker@example.com']);
        $tmpMember->generateToken();

        $this->assertStringContainsString('login/token?token', TokenAuthenticator::getTokenLink($handler, $tmpMember));
    }
}
