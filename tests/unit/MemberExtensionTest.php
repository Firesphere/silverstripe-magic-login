<?php

namespace Firesphere\MagicLogin\Tests;

use Firesphere\MagicLogin\Config\TokenConfig;
use Firesphere\MagicLogin\Extensions\MemberExtension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\SapphireTest;
use SilverStripe\Security\Member;

class MemberExtensionTest extends SapphireTest
{

    /**
     * @var TokenConfig
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = Injector::inst()->get(TokenConfig::class);
        parent::setUp();
    }

    public function testGenerateToken()
    {
        /** @var Member|MemberExtension $member */
        $member = Member::create(['Email' => 'test@example.com']);
        $member->getExtensionInstances();
        $member->generateToken();
        $this->assertNotNull($member->LoginToken);
        $this->assertGreaterThan(date('Y-m-d H:i:s'), $member->TokenExpiry);
    }
}
