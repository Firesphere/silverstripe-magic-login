<?php

namespace Firesphere\MagicLogin\Tests;

use Firesphere\MagicLogin\Config\TokenConfig;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\SapphireTest;

class TokenConfigTest extends SapphireTest
{

    /**
     * @var TokenConfig
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = Injector::inst()->get(TokenConfig::class);
        parent::setUp();
    }

    public function testConfig()
    {
        // Test the defaults
        $this->assertFalse($this->config->isAdminAccess());
        $this->assertTrue($this->config->isSameBrowser());
        $this->assertEquals(30, $this->config->getTokenLifetime());
        $this->assertFalse($this->config->isPersistentLogin());

        $this->config->setAdminAccess(true);
        $this->config->setPersistentLogin(true);
        $this->config->setSameBrowser(false);
        $this->config->setTokenLifetime(60);

        // Test it's all set properly from above.
        $this->assertTrue($this->config->isAdminAccess());
        $this->assertFalse($this->config->isSameBrowser());
        $this->assertEquals(60, $this->config->getTokenLifetime());
        $this->assertTrue($this->config->isPersistentLogin());
    }
}
