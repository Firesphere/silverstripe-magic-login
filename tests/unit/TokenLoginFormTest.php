<?php

namespace Firesphere\MagicLogin\Tests;

use Firesphere\MagicLogin\Authenticators\TokenAuthenticator;
use Firesphere\MagicLogin\Forms\TokenLoginForm;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\SapphireTest;
use SilverStripe\Forms\FieldList;

class TokenLoginFormTest extends SapphireTest
{
    /**
     * @var TokenLoginForm
     */
    protected $loginForm;

    protected function setUp(): void
    {
        $controller = Injector::inst()->get(\PageController::class);
        $authenticator = Injector::inst()->get(TokenAuthenticator::class);
        $this->loginForm = TokenLoginForm::create($controller, $authenticator, 'TokenLoginForm');
        parent::setUp();
    }

    public function testAuthenticator()
    {
        $name = _t('Firesphere\\MagicLogin\\Forms\\TokenLoginForm.TOKEN', "Magic Email Token");
        $this->assertEquals($name, $this->loginForm->getAuthenticatorName());
        $this->assertInstanceOf(TokenAuthenticator::class, $this->loginForm->getAuthenticatorClass());
    }

    public function testFormParts()
    {
        $fields = $this->loginForm->Fields();
        $this->assertInstanceOf(FieldList::class, $this->loginForm->Fields());
        $this->assertNotNull($fields->dataFieldByName('Email'));
        $this->assertNotNull($fields->dataFieldByName('AuthenticationMethod'));
        $this->assertEquals($this->loginForm->getAuthenticatorClass(), $fields->dataFieldByName('AuthenticationMethod')->dataValue());
        $this->assertInstanceOf(FieldList::class, $this->loginForm->Actions());
        $this->assertContains('Email', $this->loginForm->getValidator()->getRequired());
    }
}
